using UnityEngine;
using System.Collections;
using MqttLib;
using System;
using SimpleJSON;
using System.Collections.Generic;
using UnityEngine.UI;

public class MQTTStageController : MQTTHandlerBase {

    override protected string clientId { get { return "TARS_NEW"; } }
    public string lastTopic = "";
    public string lastMessage = "";
   
    //public List<string> stagesName;
   
	public GameObject Virus;
	public GameObject Normal;
	public GameObject Noise;
	public GameObject Black_Screen;
    
    
	
    
	
	
    public Dictionary<string, GameObject> stagesDict = new Dictionary<string, GameObject>();
   
    public void SendState(string state){
		try {
			client.Publish(Config.kTARSStateTopic, state, QoS.OnceAndOnceOnly, false);
            	
		} catch (Exception ex) {
			client.Disconnect();
		}
	}
    void Start()
    {
        Black_Screen.SetActive(false);
        
    }
    override public void Update()
    {
        base.Update();
        if(lastTopic==Config.kTARSNotificationTopic){
            if(lastMessage=="SCREEN_OFF"){
                Black_Screen.SetActive(true);
                Noise.SetActive(false);
                Virus.SetActive(false);
                Normal.SetActive(false);
            }
                
            if(lastMessage=="NOISE"){
                Noise.SetActive(true);
                Virus.SetActive(false);
                Normal.SetActive(false);
                Black_Screen.SetActive(false);
            }
            if(lastMessage=="VIRUS"){
                Noise.SetActive(false);
                Virus.SetActive(true);
                Normal.SetActive(false);
                Black_Screen.SetActive(false);
            }
            if(lastMessage=="NORMAL"){
                Noise.SetActive(false);
                Virus.SetActive(false);
                Normal.SetActive(true);
                Black_Screen.SetActive(false);
            }
        }
            
		if (lastTopic == Config.kTARSResetTopic)
		{
			if(lastMessage=="RESET")
			{
				Application.LoadLevel("Main");
			}
		}
        
        if(lastTopic!="") lastTopic="";

            
    }
    public bool isMQTTConnect() {
        return client.IsConnected;
    }

        // событие успешного подключения
    override protected void OnConnect(object sender, EventArgs e)
    {
        base.OnConnect(sender, e);
        client.Subscribe(Config.kTARSResetTopic, QoS.OnceAndOnceOnly);
        client.Subscribe(Config.kTARSNotificationTopic, QoS.OnceAndOnceOnly);
        
		
		
    }

    // событие получение сообщения
    override protected bool OnMessageArrived(object sender, PublishArrivedArgs e)
    {
        base.OnMessageArrived(sender, e);
        bool result = base.OnMessageArrived(sender, e);
        lastTopic = e.Topic.ToString();
        lastMessage = e.Payload.ToString();

        return result;
    }

}
