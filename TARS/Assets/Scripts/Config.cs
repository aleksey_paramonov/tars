﻿using System;

public static class Config
{
	// параметры доступа к серверу

	public const string kConnectionString = "tcp://10.1.10.17:1883";
	
	public const string kTARSNotificationTopic = "ufo/Bridge_TARS_Notification/commands";
	
	public const string kTARSResetTopic = "ufo/Bridge_TARS/commands";
	public const string kTARSStateTopic = "ufo/Bridge_TARS/state";
	


}

