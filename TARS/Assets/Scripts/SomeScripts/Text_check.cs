﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class Text_check : MonoBehaviour {
	
	public Text Text;
	public float Period;
	public int ComaCount;
	public Text_check NextText;
	public bool start;
	public bool working;
	
	// Use this for initialization
	void Start () {

		start = false;
		working = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (start&&working==false) StartCoroutine (AddComas ());
	}



	public void Begin()
	{
		start = true;
	}

	IEnumerator AddComas()
	{
		working = true;
		if (start)
		{
			for (int i = 0; i <ComaCount; i++) {
				Text.text = Text.text + ".";

				if (i == ComaCount - 1)
					{
						Text.text = Text.text + "ОК";
					if(!Object.ReferenceEquals(null, NextText)) NextText.Begin();
			
				}
				yield return new WaitForSeconds (Period);
			}

		}
	}
}
