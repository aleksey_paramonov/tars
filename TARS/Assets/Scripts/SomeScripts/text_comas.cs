﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class text_comas : MonoBehaviour {

	public Text Text;
	public Text_check LeftBlock;
	public Text_check RightBlock;
	public GameObject Screen;
    public GameObject nextScreen;

    // Use this for initialization
    void Start () {

		StartCoroutine (AddComas ());
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Text.text.Contains ("ОК"))
			DisableScreen ();
	
	}

	IEnumerator AddComas()
	{
		for (int i = 0; i <7; i++) {
			Text.text = Text.text + ".";
			//if (i == 6) Text.text = Text.text + "ОК";
			yield return new WaitForSeconds(2f);
			LeftBlock.Begin();
			RightBlock.Begin();
		}

	}

	public void DisableScreen()
	{
		//yield return new WaitForSeconds(2f);
		Screen.SetActive (false);
        nextScreen.SetActive(true);
        //gameObject.GetComponent<MQTTStageController>().LoadStage("EnterSample");
	}

}
