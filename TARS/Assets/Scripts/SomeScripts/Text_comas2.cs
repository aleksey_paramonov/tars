﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Text_comas2 : MonoBehaviour {

	public Text Text;
	public GameObject Screen;

	// Use this for initialization
	void Start () {

		StartCoroutine (AddComas ());
	
	}

	
	IEnumerator AddComas()
	{
		for (int i = 0; i <24; i++) {
			Text.text = Text.text + ".";
			yield return new WaitForSeconds(0.3f);
			if (i == 23) ActivateScreen();
		}
		
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void ActivateScreen()
	{
		//yield return new WaitForSeconds(2f);
		Screen.SetActive(true);
	}
}
