﻿using UnityEngine;
using System.Collections;

public class GameobjectActive : MonoBehaviour {

	public GameObject Turnoff1;
	public GameObject Turnoff2;
	public GameObject Turnoff3;
	public GameObject Turnoff4;
	// Use this for initialization
	void Start () {

		StartCoroutine (AddComas());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	IEnumerator AddComas()
	{
		for (;;) {

			if (Turnoff1.activeSelf) {
				Turnoff1.SetActive (false);
				Turnoff2.SetActive (false);
				Turnoff3.SetActive (false);
				Turnoff4.SetActive (false);
			} else {
				Turnoff1.SetActive (true);
				Turnoff2.SetActive (true);
				Turnoff3.SetActive (true);
				Turnoff4.SetActive (true);
			}
			Debug.Log ("working");
			yield return new WaitForSeconds (1.5f);
		}
	}
}
